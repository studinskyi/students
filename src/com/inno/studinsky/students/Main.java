package com.inno.studinsky.students;

import com.inno.studinsky.students.managers.Manager;
import com.inno.studinsky.students.models.Group;
import com.inno.studinsky.students.models.Journal;
import com.inno.studinsky.students.models.Lesson;
import com.inno.studinsky.students.models.Student;
import com.inno.studinsky.students.utils.ProjectUtilits;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

public class Main {
    private static Manager manager = new Manager();
    private static ProjectUtilits pUtil = new ProjectUtilits();

    public static void main(String[] args) {
        // создание объектов групп
        Group gr1 = manager.addGroup("группа 1", 1);
        Group gr2 = manager.addGroup("группа 2", 2);
        Group gr3 = manager.addGroup("группа 3", 3);
        List<Group> groups = new ArrayList<>();
        for (HashMap.Entry<Long, Group> entry : manager.getGroups().entrySet()) {
            System.out.println("группа " + entry.getValue().toString());
            groups.add(entry.getValue());
        }

        // 1-й вариант добавления нового студента в группу
        manager.addNewStudentToGroup(pUtil.calend(1994, Calendar.FEBRUARY, 8), "Petr", "Petrov", "Petrovich", gr1);
        manager.addNewStudentToGroup(pUtil.calend(1980, Calendar.NOVEMBER, 25), "Ivan", "Ivanov", "Ivanovich", gr2);
        manager.addNewStudentToGroup(pUtil.calend(1997, Calendar.MAY, 16), "Alexandr", "Alexandrov", "Alexandrovich", gr2);
        manager.addNewStudentToGroup(pUtil.calend(1975, Calendar.APRIL, 14), "Viktor", "Viktorov", "Viktorovich", gr3);

        // вывод групп и студентов
        System.out.println("вывод групп и студентов:");
        List<Student> students = new ArrayList<>();
        for (HashMap.Entry<Long, Group> entry : manager.getGroups().entrySet())
            for (HashMap.Entry<Long, Student> entrySt : entry.getValue().getStudents().entrySet()) {
                System.out.println("студент " + entrySt.getValue().toString() + " ; группа " + entry.getValue().toString());
                students.add(entrySt.getValue());
            }

        // создание объектов занятий
        Lesson les1 = manager.addLesson("История", pUtil.calend(2017, Calendar.JUNE, 10), 60, "кабинет 101", "История древних времен", "Древняя Русь", "Сидоров И.П.", gr1);
        Lesson les2 = manager.addLesson("Философия", pUtil.calend(2017, Calendar.AUGUST, 16), 60, "кабинет 202", "Труды немецких философов", "Философия капитала", "Карл Маркс", gr2);
        // вывод списка занятий
        List<Lesson> lessons = new ArrayList<>();
        System.out.println("вывод списка занятий:");
        for (HashMap.Entry<Long, Lesson> entry : manager.getLessons().entrySet()) {
            System.out.println("занятие " + entry.getValue().toString());
            lessons.add(entry.getValue());
        }


        // создание объектов журналов посещения
        Journal journ1 = manager.addJournal("журнал - История", les1);
        manager.addStudentsToJournal(journ1, gr1.getStudents());
        Journal journ2 = manager.addJournal("журнал - Философия", les2);
        manager.addStudentsToJournal(journ1, gr2.getStudents());
        manager.addStudentsToJournal(journ1, gr3.getStudents());
        // вывод списка журналов посещения
        List<Journal> journals = new ArrayList<>();
        System.out.println("вывод списка журналов посещения:");
        for (HashMap.Entry<Long, Journal> entry : manager.getJournals().entrySet()) {
            System.out.println("журнал " + entry.getValue().toString());
            journals.add(entry.getValue());
        }

        // запись структуры данных в xml-файл (при помощи объекта DocumentBuilderFactory)
        serialize_toXML_Groups(groups);
        serialize_toXML_Students(students);
        serialize_toXML_Lessons(lessons);
        serialize_toXML_Journals(journals);

        // пример сериализации через объект ObjectOutputStream
        //testSerializeListStudents();
    }

    public static void serialize_toXML_Students(List<Student> students) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element general_xml = doc.createElement("Students");
            doc.appendChild(general_xml);

            for (Student student : students) {
                Element elem_xml = doc.createElement("Student");
                general_xml.appendChild(elem_xml);

                Element fieldsxml = doc.createElement("Fields");
                elem_xml.appendChild(fieldsxml);

                for (Field field : Student.class.getDeclaredFields()) {
                    Element carname = doc.createElement("Field");

                    Attr attrName = doc.createAttribute("Name");
                    attrName.setValue(field.getName());
                    carname.setAttributeNode(attrName);

                    Attr attrType = doc.createAttribute("type");
                    attrType.setValue(field.getType().getName());
                    carname.setAttributeNode(attrType);

                    field.setAccessible(true);
                    Attr attrValue = doc.createAttribute("value");
                    attrValue.setValue(field.get(student).toString());
                    carname.setAttributeNode(attrValue);

                    carname.appendChild(doc.createTextNode(field.getName()));
                    fieldsxml.appendChild(carname);

                    System.out.println(field.getName());
                    //                    System.out.println(field.getName());
                    //                    System.out.println(field.getType().getName());
                    //                    System.out.println(field.isAccessible());
                }
            }

            // запись полученной структуры данных в xml-файл
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("c:\\test_QA\\output_XML\\students_XML.xml"));
            transformer.transform(source, result);
            // вывод результирующего текста xml, помещенного в файл
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public static void serialize_toXML_Journals(List<Journal> journals) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element general_xml = doc.createElement("Journals");
            doc.appendChild(general_xml);

            for (Journal journal : journals) {
                Element elem_xml = doc.createElement("Journal");
                general_xml.appendChild(elem_xml);

                Element fieldsxml = doc.createElement("Fields");
                elem_xml.appendChild(fieldsxml);

                for (Field field : Journal.class.getDeclaredFields()) {
                    Element carname = doc.createElement("Field");

                    Attr attrName = doc.createAttribute("Name");
                    attrName.setValue(field.getName());
                    carname.setAttributeNode(attrName);

                    Attr attrType = doc.createAttribute("type");
                    attrType.setValue(field.getType().getName());
                    carname.setAttributeNode(attrType);

                    field.setAccessible(true);
                    Attr attrValue = doc.createAttribute("value");
                    attrValue.setValue(field.get(journal).toString());
                    carname.setAttributeNode(attrValue);

                    carname.appendChild(doc.createTextNode(field.getName()));
                    fieldsxml.appendChild(carname);

                    System.out.println(field.getName());
                    //                    System.out.println(field.getName());
                    //                    System.out.println(field.getType().getName());
                    //                    System.out.println(field.isAccessible());
                }
            }

            // запись полученной структуры данных в xml-файл
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("c:\\test_QA\\output_XML\\journals_XML.xml"));
            transformer.transform(source, result);
            // вывод результирующего текста xml, помещенного в файл
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public static void serialize_toXML_Groups(List<Group> groups) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element general_xml = doc.createElement("Groups");
            doc.appendChild(general_xml);

            for (Group group : groups) {
                Element elem_xml = doc.createElement("Group");
                general_xml.appendChild(elem_xml);

                Element fieldsxml = doc.createElement("Fields");
                elem_xml.appendChild(fieldsxml);

                for (Field field : Group.class.getDeclaredFields()) {
                    Element carname = doc.createElement("Field");

                    Attr attrName = doc.createAttribute("Name");
                    attrName.setValue(field.getName());
                    carname.setAttributeNode(attrName);

                    Attr attrType = doc.createAttribute("type");
                    attrType.setValue(field.getType().getName());
                    carname.setAttributeNode(attrType);

                    field.setAccessible(true);
                    Attr attrValue = doc.createAttribute("value");
                    attrValue.setValue(field.get(group).toString());
                    carname.setAttributeNode(attrValue);

                    carname.appendChild(doc.createTextNode(field.getName()));
                    fieldsxml.appendChild(carname);

                    System.out.println(field.getName());
                    //                    System.out.println(field.getName());
                    //                    System.out.println(field.getType().getName());
                    //                    System.out.println(field.isAccessible());
                }
            }

            // запись полученной структуры данных в xml-файл
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("c:\\test_QA\\output_XML\\groups_XML.xml"));
            transformer.transform(source, result);
            // вывод результирующего текста xml, помещенного в файл
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public static void serialize_toXML_Lessons(List<Lesson> lessons) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element general_xml = doc.createElement("Lessons");
            doc.appendChild(general_xml);

            for (Lesson lesson : lessons) {
                Element elem_xml = doc.createElement("Lesson");
                general_xml.appendChild(elem_xml);

                Element fieldsxml = doc.createElement("Fields");
                elem_xml.appendChild(fieldsxml);

                for (Field field : Lesson.class.getDeclaredFields()) {
                    Element carname = doc.createElement("Field");

                    Attr attrName = doc.createAttribute("Name");
                    attrName.setValue(field.getName());
                    carname.setAttributeNode(attrName);

                    Attr attrType = doc.createAttribute("type");
                    attrType.setValue(field.getType().getName());
                    carname.setAttributeNode(attrType);

                    field.setAccessible(true);
                    Attr attrValue = doc.createAttribute("value");
                    attrValue.setValue(field.get(lesson).toString());
                    carname.setAttributeNode(attrValue);

                    carname.appendChild(doc.createTextNode(field.getName()));
                    fieldsxml.appendChild(carname);

                    System.out.println(field.getName());
                    //                    System.out.println(field.getName());
                    //                    System.out.println(field.getType().getName());
                    //                    System.out.println(field.isAccessible());
                }
            }

            // запись полученной структуры данных в xml-файл
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("c:\\test_QA\\output_XML\\lessons_XML.xml"));
            transformer.transform(source, result);
            // вывод результирующего текста xml, помещенного в файл
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public static void testSerializeListStudents() {
        String fullPathDirectory = ProjectUtilits.setWorkFolder("c:\\test_QA\\");
        //SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);

        List<Student> list = new ArrayList<>();
        //        try {
        //            list.addGroup(new Student(dateformat.parse("08-02-1994"), 1l, "Petr", "Petrov", "Petrovich"));
        //            list.addGroup(new Student(dateformat.parse("25-11-1980"), 2l, "Ivan", "Ivanov", "Ivanovich"));
        //            list.addGroup(new Student(dateformat.parse("16-05-1997"), 2l, "Alexandr", "Alexandrov", "Alexandrovich"));
        //        } catch (ParseException e) {
        //            //e.printStackTrace();
        //            System.out.println("Произошла ошибка парсинга даты рождения студента.");
        //        }
        list.add(new Student(new GregorianCalendar(1994, Calendar.FEBRUARY, 8), 1l, "Petr", "Petrov", "Petrovich"));
        list.add(new Student(new GregorianCalendar(1980, Calendar.NOVEMBER, 25), 2l, "Ivan", "Ivanov", "Ivanovich"));
        list.add(new Student(new GregorianCalendar(1997, Calendar.MAY, 16), 2l, "Alexandr", "Alexandrov", "Alexandrovich"));
        list.add(new Student(new GregorianCalendar(1975, Calendar.APRIL, 14), 3l, "Viktor", "Viktorov", "Viktorovich"));

        serializeListToFile(list, fullPathDirectory, "test.txt");
        List<Student> list2 = (List<Student>) deserializeFromFile(fullPathDirectory, "test.txt");
        for (Student elem : list2)
            System.out.println("" + elem.toString());
    }

    public static void serializeListToFile(List<?> list, String fullPathDirectory, String fileName) {
        try {
            FileOutputStream out = new FileOutputStream(fullPathDirectory + fileName);
            ObjectOutputStream oout = new ObjectOutputStream(out);
            oout.writeObject(list);
            oout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<?> deserializeFromFile(String fullPathDirectory, String fileName) {
        List<?> list2 = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fullPathDirectory + fileName));
            list2 = (List<?>) ois.readObject();
            //            for (Student elem : list2)
            //                System.out.println("" + elem.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return list2;
    }


}

