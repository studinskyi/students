package com.inno.studinsky.students.managers;

import com.inno.studinsky.students.models.Group;
import com.inno.studinsky.students.models.Journal;
import com.inno.studinsky.students.models.Student;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ManagerGroups implements ManagerInterface {
    private Manager manager;
    private HashMap<Long, Group> groups;

    public ManagerGroups(Manager manager) {
        this.manager = manager;
        this.groups = new HashMap<>();
    }

    public Group addGroup(String name, int numberGroup) {
        Group groupNew;
        Group groupFound = findGroup(numberGroup, name);
        if (groupFound == null) {
            groupNew = new Group(name, numberGroup);
            groups.put(groupNew.getId(), groupNew);
        } else
            groupNew = groupFound;

        return groupNew;
    }

    public Group findGroup(int numberGroup) {
        //Group groupFound = null;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getNumberGroup() == numberGroup)
                return entry.getValue();

        return null; // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(int numberGroup, String name) {
        //Group groupFound = null;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getName().equals(name) && entry.getValue().getNumberGroup() == numberGroup)
                return entry.getValue();

        return null; // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(String name) {
        //Group groupFound = null;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getName().equals(name))
                return entry.getValue();

        return null; // возвращает null если группа по искомому полю не была найдена
    }

    public Long getGroupIdByNumberGroup(int numberGroup) {
        //Long idGroup = 0l;
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            if (entry.getValue().getNumberGroup() == numberGroup)
                return entry.getKey();

        return (long) 0; // возвращает (long) 0 если группа по искомому полю не была найдена
    }

    public HashMap<Long, Group> getGroups() {
        return groups;
    }

    @Override
    // переопределён для использования в Proxy и Mock тестах
    public Object addGroup(Object obj, int numberGroup) {
        return addGroup((String) obj, numberGroup);
    }
}
