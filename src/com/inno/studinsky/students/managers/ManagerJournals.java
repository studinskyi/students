package com.inno.studinsky.students.managers;

import com.inno.studinsky.students.models.Journal;
import com.inno.studinsky.students.models.Lesson;
import com.inno.studinsky.students.models.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManagerJournals {
    private Manager manager;
    private HashMap<Long, Journal> journals;

    public ManagerJournals(Manager manager) {
        this.manager = manager;
        this.journals = new HashMap<>();
    }


    public Journal addJournal(String name, Lesson lesson) {
        Journal journalNew;
        Journal journalFound = findJournal(name, lesson);
        if (journalFound == null) {
            journalNew = new Journal(name, lesson);
            journals.put(journalNew.getId(),journalNew);
        } else
            journalNew = journalFound;

        return journalNew;
    }

    public Journal addJournal(String name, Lesson lesson, HashMap<Long, Student> students) {
        Journal journalNew;
        Journal journalFound = findJournal(name, lesson);
        if (journalFound == null) {
            journalNew = new Journal(name, lesson);
            addStudentsToJournal(journalNew, students);
            journals.put(journalNew.getId(),journalNew);
        } else
            journalNew = journalFound;

        addStudentsToJournal(journalNew, students); // добавление списка студентов в новый журнал

        return journalNew;
    }

    public Journal findJournal(String name, Lesson lesson) {
        Journal journalFound = null;
        for (HashMap.Entry<Long,Journal> entry : journals.entrySet()) {
            if(entry.getValue().getName().equals(name) && entry.getValue().getLesson().equals(lesson))
                journalFound = entry.getValue();
        }

        return journalFound;
    }

    // добавление одного конкретно взятого студента в журнал
    public boolean addStudentToJournal(Journal journal, Student student) {
        boolean successfulAdd = false; // был ли добавлен элемент
        HashMap<Student, Boolean> studentsInJournal = journal.getPresentStudents();
        if (!studentsInJournal.containsKey(student)) {
            studentsInJournal.put(student, false);
            successfulAdd = true;
        }

        return successfulAdd;
    }

    // добавление списка студентов в журнал (сюда же входит вариант добавления списка студентов группы)
    public HashMap<Student, Boolean> addStudentsToJournal(Journal journal, HashMap<Long, Student> students) {
        HashMap<Student, Boolean> studentsInJournal = journal.getPresentStudents();
        if (students.size() > 0) {
            for (HashMap.Entry<Long, Student> entry : students.entrySet())
                addStudentToJournal(journal, entry.getValue());
                //            for (Student student : listStudents)
                //                addStudentToJournal(journal, student);
        }
        return studentsInJournal; // возврат результирующего HashMap присутствия студентов
    }

    public Manager getManager() {
        return manager;
    }

    public HashMap<Long, Journal> getJournals() {
        return journals;
    }
}
