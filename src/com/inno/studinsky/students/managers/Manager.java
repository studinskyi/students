package com.inno.studinsky.students.managers;

import com.inno.studinsky.students.models.Group;
import com.inno.studinsky.students.models.Journal;
import com.inno.studinsky.students.models.Lesson;
import com.inno.studinsky.students.models.Student;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Manager implements ManagerInterface {
    private ManagerGroups mGroups;
    private ManagerJournals mJournals;
    private ManagerLessons mLessons;
    private ManagerStudents mStudents;

    public Manager() {
        mGroups = new ManagerGroups(this);
        mJournals = new ManagerJournals(this);
        mLessons = new ManagerLessons(this);
        mStudents = new ManagerStudents(this);
    }

    public Group addGroup(String name, int number) {
        return mGroups.addGroup(name, number);
    }

    @Override
    public Object addGroup(Object obj, int numberGroup) {
        // переопределённый для использования в Proxy тестах
        return mGroups.addGroup((String) obj, numberGroup);
    }

    public Group findGroup(int numberGroup) {
        return mGroups.findGroup(numberGroup); // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(int numberGroup, String name) {
        return mGroups.findGroup(numberGroup, name); // возвращает null если группа по искомому полю не была найдена
    }

    public Group findGroup(String name) {
        return mGroups.findGroup(name); // возвращает null если группа по искомому полю не была найдена
    }

    public Long getGroupIdByNumberGroup(int numberGroup) {
        return mGroups.getGroupIdByNumberGroup(numberGroup); // возвращает (long) 0 если группа по искомому полю не была найдена
    }

    public HashMap<Long, Group> getGroups() {
        return mGroups.getGroups();
    }

    public int getAmountOfStudnts() {
        return mStudents.getAmountOfStudnts();
    }

    //    public Student addStudent(Calendar dateOfBirth, Long groupID, String firstName,
    //                              String surname, String secondName) {
    //        return mStudents.addStudent(dateOfBirth, groupID, firstName, surname, secondName);
    //    }

    public Student addNewStudentToGroup(Calendar dateOfBirth, String firstName,
                                        String surname, String secondName, Group group) {
        return mStudents.addNewStudentToGroup(dateOfBirth, firstName, surname, secondName, group);
    }

    public Student getStudent(String firstName, String surname, String secondName) {
        return mStudents.getStudent(firstName, surname, secondName);
    }

    public void addStudentToGroup(Student student, Group group) {
        mStudents.addStudentToGroup(student, group);
    }

    // добавление списка студентов в гуппу
    public HashMap<Long, Student> addStudentsToGroup(Group group, List<Student> listStudents) {
        return mStudents.addStudentsToGroup(group, listStudents); // возврат результирующего HashMap студентов группы
    }

    // функция проверки наличия студента в группе
    public boolean isStudentInGroup(Group group, Student student) {
        return mStudents.isStudentInGroup(group, student);
    }

    public void removeStudent(Student student) {
        mStudents.removeStudent(student);
    }

    public Lesson addLesson(String name, Calendar startTime, Integer duration,
                            String room, String description, String subject, String lector, Group group) {
        return mLessons.addLesson(name, startTime, duration, room, description, subject, lector, group);
    }

    public HashMap<Long, Lesson> addLesson(Lesson lesson) {
        return mLessons.addLesson(lesson);
    }

    public HashMap<Long, Lesson> getLessons() {
        return mLessons.getLessons();
    }

    public Journal addJournal(String name, Lesson lesson) {
        return mJournals.addJournal(name, lesson);
    }

    public Journal addJournal(String name, Lesson lesson, List<Student> students) {
        return mJournals.addJournal(name, lesson);
    }

    public Journal findJournal(String name, Lesson lesson) {
        return mJournals.findJournal(name, lesson);
    }

    // добавление одного конкретно взятого студента в журнал
    public boolean addStudentToJournal(Journal journal, Student student) {
        return mJournals.addStudentToJournal(journal, student);
    }

    // добавление списка студентов в журнал (сюда же входит вариант добавления списка студентов группы)
    public HashMap<Student, Boolean> addStudentsToJournal(Journal journal, HashMap<Long, Student> students) {
        return mJournals.addStudentsToJournal(journal, students); // возврат результирующего HashMap присутствия студентов
    }

    public HashMap<Long, Journal> getJournals() {
        return mJournals.getJournals();
    }

    public ManagerGroups get_mGroups() {
        return mGroups;
    }

    public ManagerJournals get_mJournals() {
        return mJournals;
    }

    public ManagerLessons get_mLessons() {
        return mLessons;
    }

    public ManagerStudents get_mStudents() {
        return mStudents;
    }
}
