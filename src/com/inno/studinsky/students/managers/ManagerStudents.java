package com.inno.studinsky.students.managers;

import com.inno.studinsky.students.models.Group;
import com.inno.studinsky.students.models.Student;

import java.util.*;

public class ManagerStudents {
    private Manager manager;

    public ManagerStudents(Manager manager) {
        this.manager = manager;
    }

    public int getAmountOfStudnts() {
        int counStudents = 0;
        HashMap<Long, Group> groups = manager.get_mGroups().getGroups();
        // проход по всем группам и накопление их студентов
        for (HashMap.Entry<Long, Group> entry : groups.entrySet())
            counStudents = counStudents + entry.getValue().getStudents().size();

        return counStudents;
    }

    public Student addStudent(Calendar dateOfBirth, Long groupID, String firstName,
                              String surname, String secondName) {
        // если студент с этим ФИО не найден то можно добавить его
        Student studentNew = getStudent(firstName, surname, secondName);
        if (studentNew == null)
            studentNew = new Student(dateOfBirth, groupID, firstName, surname, secondName);

        return studentNew; // если студент уже имелся в базе, то возвращается ссылка на этот объект
    }

    public Student addNewStudentToGroup(Calendar dateOfBirth, String firstName,
                                        String surname, String secondName, Group group) {
        Student studentNew = addStudent(dateOfBirth, group.getId(), firstName, surname, secondName);
        if (studentNew != null)
            addStudentToGroup(studentNew, group);
        return studentNew;
    }

    //    public boolean addStudent(Student student, int groupNum) {
    //        Long groupId = getGroupId(groupNum);
    //        if (groupId == -1) return false;
    //        for (Group group : manager.get_mGroups().getGroups()) {
    //            if (group.getId().equals(groupId)) {
    //                addStudent(student, group);
    //                return true;
    //            }
    //        }
    //        return false;
    //    }

    public Student getStudent(String firstName, String surname, String secondName) {
        Student student;
        HashMap<Long, Group> groups = manager.get_mGroups().getGroups();
        // проход по всем группам и сверка ФИО студента
        for (HashMap.Entry<Long, Group> entryGr : groups.entrySet())
            for (HashMap.Entry<Long, Student> entrySt : entryGr.getValue().getStudents().entrySet()) {
                student = entrySt.getValue();
                if (student.getFirstName().equals(firstName) &&
                        student.getSecondName().equals(secondName) &&
                        student.getSurname().equals(surname))
                    return student;
            }

        return null;
    }

    public void addStudentToGroup(Student student, Group group) {
        //        Group groupCurrent = manager.get_mGroups().getGroups().get(student.getGroupID());
        //        if (groupCurrent.equals(group)) // если студент уже находится в этой группе то делать ничего не нужно
        //            return;

        removeStudentFromGroup(student, group); // исключение студента из старой группы
        student.setGroupID(group.getId()); // установка id-группы в поле groupID студента
        group.getStudents().put(student.getId(), student);
        //        List<Student> studentsInGroup = group.getStudents();
        //        if (!studentsInGroup.contains(student)) {
        //            studentsInGroup.add(student);
        //            successfulAdd = true;
        //        }
    }

    // добавление списка студентов в гуппу
    public HashMap<Long, Student> addStudentsToGroup(Group group, List<Student> listStudents) {
        HashMap<Long, Student> studentsInGroup = group.getStudents();
        if (listStudents.size() > 0) {
            for (Student student : listStudents)
                addStudentToGroup(student, group);
        }
        return group.getStudents(); // возврат результирующего HashMap студентов группы
    }


    // функция проверки наличия студента в группе
    public boolean isStudentInGroup(Group group, Student student) {
        HashMap<Long, Student> students = group.getStudents();
        if (students.get(student.getId()) != null)
            return true; // студент присутствует в списке группы
        return false; // студент отсутствует в списке группы
    }


    public void removeStudent(Student student) {
        //for (Group group : manager.get_mGroups().getGroups()) {
        Group group = manager.get_mGroups().getGroups().get(student.getGroupID());
        group.getStudents().remove(student.getId());
        //        HashMap<Long, Student> students = manager.get_mGroups().getGroups().get(student.getGroupID()).getStudents();
        //        for (HashMap.Entry<Long, Student> entrySt : students.entrySet()) {
        //            if (student.equals(entrySt.getValue())) { // объект студента найден в списке группы
        //                students.remove(student.getId());
        //                return true; // истина в случае удаления студента из списка
        //            }
        //        }
        //        return false;
    }

    private void removeStudentFromGroup(Student student, Group group) {
        group.getStudents().remove(student.getId());
    }

    @Override
    public String toString() {
        return manager.get_mGroups().getGroups().toString();
    }
}
