package com.inno.studinsky.students.managers;

import com.inno.studinsky.students.models.Group;
import com.inno.studinsky.students.models.Lesson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ManagerLessons {
    private Manager manager;
    private HashMap<Long, Lesson> lessons;

    public ManagerLessons(Manager manager) {
        this.manager = manager;
        this.lessons = new HashMap<>();
    }

    public Lesson addLesson(String name, Calendar startTime, Integer duration,
                            String room, String description, String subject, String lector, Group group) {
        Lesson lesson = new Lesson(name, startTime, duration, room, description, subject, lector, group);
        //        // добавление лессона, если он ещё не зафиксирован
        //if (lesson != null && lessons.get(lesson.getId()) == null)
        this.lessons.put(lesson.getId(), lesson);
        return lesson;
    }

    public HashMap<Long, Lesson> addLesson(Lesson lesson) {
        //if (lesson != null && lessons.get(lesson.getId()) == null)
        this.lessons.put(lesson.getId(), lesson);
        return lessons;
    }

    public Manager getManager() {
        return manager;
    }

    public HashMap<Long, Lesson> getLessons() {
        return lessons;
    }
}
