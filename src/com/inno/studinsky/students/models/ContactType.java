package com.inno.studinsky.students.models;

public enum ContactType {
    PHONE,
    EMAIL,
    TELEGRAM,
    SKYPE,
    VK,
    FACEBOOK,
    LINKEDIN,
    ODNOKLASNIKI,
}
