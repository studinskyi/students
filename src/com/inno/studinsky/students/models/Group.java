package com.inno.studinsky.students.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Group {
    private Long id;
    private UUID SerialVersionUUID;
    private String name;
    private int numberGroup;
    //private List<Student> students;
    private HashMap<Long, Student> students;

    public Group(String name, int numberGroup) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.name = name;
        this.numberGroup = numberGroup;
        this.students = new HashMap<>();
    }

    public Group(String name, int numberGroup, HashMap<Long, Student> students) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.name = name;
        this.numberGroup = numberGroup;
        this.students = students;
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Group))
            return false;

        if (this.id != ((Group) obj).getId()) {
            // проверка идентичности группы по полю id
            return false;
        }

        return true;
        //return super.equals(obj);
    }

    @Override
    public String toString() {
        return "id = " + id +
                "; SerialVersionUUID = " + SerialVersionUUID +
                "; name = " + name +
                "; numberGroup = " + numberGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<Long, Student> getStudents() {
        return students;
    }

    public void setStudents(HashMap<Long, Student> students) {
        this.students = students;
    }

    public int getNumberGroup() {
        return numberGroup;
    }

    public Long getId() {
        return id;
    }

    public UUID getSerialVersionUUID() {
        return SerialVersionUUID;
    }
}
