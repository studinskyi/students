package com.inno.studinsky.students.models;


import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Journal {
    private Long id;
    private UUID SerialVersionUUID;
    private String name;
    private Lesson lesson;
    private HashMap<Student, Boolean> presentStudents; // где Long описывает id студента, а тип Boolean - присутствие его на лекции

    public Journal(String name, Lesson lesson) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.name = name;
        this.lesson = lesson;
        this.presentStudents = new HashMap<>();
    }

    //    public Journal(String name, HashMap<Long, Student> students, Lesson lesson) {
    //        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
    //        this.name = name;
    //        this.lesson = lesson;
    //        this.presentStudents = new HashMap<>();
    //        // добавление студентов при их наличии в передаваемом списке
    //        if (students.size() > 0) {
    //            for (HashMap.Entry<Long, Student> entry : students.entrySet())
    //                addStudentToJournal(journal, entry.getValue());
    //            //            for (Student student : students)
    //            //                if (!students.contains(student))
    //            //                    this.presentStudents.put(student, false);
    //        }
    //    }


    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        if (obj == null)
            return false;

        if (!(obj instanceof Journal))
            return false;

        if (this.id != ((Journal) obj).getId()) {
            // проверка идентичности Journal по полю id
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "id = " + id +
                "; SerialVersionUUID = " + SerialVersionUUID +
                "; name = " + name +
                "; lesson = " + lesson;
    }

    public HashMap<Student, Boolean> getPresentStudents() {
        return presentStudents;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public UUID getSerialVersionUUID() {
        return SerialVersionUUID;
    }
}
