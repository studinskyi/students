package com.inno.studinsky.students.models;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

//@Deprecated
public class Student implements Serializable {
    private Long id;
    private UUID SerialVersionUUID;
    private Calendar dateOfBirth;
    private Long groupID; // связь множество студентов к одной группе (каждый студент может быть привязан к одной группе)
    // ФИО человека
    private String firstName;
    private String surname;
    private String secondName;
    // контакты студента
    private List<Contact> contacts;

    public Student(Calendar dateOfBirth, Long groupID, String firstName,
                   String surname, String secondName) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100); //(new Random()).nextInt(100)
        this.SerialVersionUUID = UUID.randomUUID();
        this.dateOfBirth = dateOfBirth;
        this.groupID = groupID;
        this.firstName = firstName;
        this.surname = surname;
        this.secondName = secondName;
        this.contacts = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        if (obj == null)
            return false;

        if (!(obj instanceof Student))
            return false;

        if (this.id != ((Student) obj).getId()) {
            // проверка идентичности студента по полю id
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public Long getId() {
        return id;
    }

    public Calendar getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Long getGroupID() {
        return groupID;
    }

    public void setGroupID(Long groupID) {
        this.groupID = groupID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public UUID getSerialVersionUUID() {
        return SerialVersionUUID;
    }

    @Override
    public String toString() {
        SimpleDateFormat format1 = new SimpleDateFormat("dd MM yyyy");
        return "id = " + id +
                "; SerialVersionUUID = " + SerialVersionUUID.toString() +
                "; dateOfBirth = " + format1.format(dateOfBirth.getTime()) +
                "; groupID = " + groupID +
                "; firstName = " + firstName +
                "; surname = " + surname +
                "; secondName = " + secondName;
    }

}
