package com.inno.studinsky.students.models;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.swing.event.DocumentEvent;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class MainXML {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student(new GregorianCalendar(1994, Calendar.FEBRUARY, 8), 1l, "Conor", "John", "Reese"));
        students.add(new Student(new GregorianCalendar(1991, Calendar.APRIL, 24), 2l, "Petr", "Petrov", "Petrovich"));
        students.add(new Student(new GregorianCalendar(1983, Calendar.MAY, 15), 3l, "Ivan", "Ivanov", "Ivanovich"));

        //addStudentToXML(Document document, Student student);

        try {
            DocumentBuilderFactory dbf =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document document = docBuilder.getDOMImplementation().
                    createDocument("", "students", null);

            System.out.println("список студентов для сериализации и выгрузки в XML: ");
            for (Student student : students) {
                System.out.println(student.toString());

                    //                // Вызываем метод для добавления студента в XML
                    //                addStudentToXML(document, student);


            }



        } catch (ParserConfigurationException ex) {
            ex.printStackTrace(System.out);
        }

//        String strRezultXML = "";
//        strRezultXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";



//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); //создали фабрику строителей, сложный и грамосткий процесс (по реже выполняйте это действие)
//        // f.setValidating(false); // не делать проверку валидации
//        DocumentBuilder db = null; // создали конкретного строителя документа
//        try {
//            db = dbf.newDocumentBuilder();
//        } catch (ParserConfigurationException e) {
//            e.printStackTrace();
//        }
//        Document doc = null; // стооитель построил документ
//        try {
//            doc = db.parse(new File("c:\\test_QA\\sample_xml.xml"));
//        } catch (SAXException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        //Document - тоже является нодом, и импленментирует методы
//        visit(doc, 0);

    }

    //    private static void addStudentToXML(Document document, Student student) {
    //
    //        // Получаем корневой элемент
    //        Node root = document.getDocumentElement();
    //
    //        System.out.println("");
    //        System.out.println("fields элемента: " + student.getClass().getName());
    //        for (Field f : student.getClass().getDeclaredFields()) {
    //            System.out.println(f.getName() + " " + f.getType().toString());
    //        //            // Создаем нового студента по элементам
    //                    Element student = document.createElement("Book");
    //        //            Element title = document.createElement("Title");
    //
    //        }
    //
    //
    //
    //        //        System.out.println("");
    //        //        System.out.println("metods элемента: " + student.getClass().getName());
    //        //        for (Method method : student.getClass().getDeclaredMethods()) {
    //        //            System.out.println(method.getName() + " " + method.getReturnType().toString() + " " + method.getParameterTypes().length);
    //        //        }
    //
    //
    //        // Записываем XML в файл
    //        writeDocument(document);
    //
    //    }

    // Функция для сохранения DOM в файл
    private static void writeDocument(Document document) throws TransformerFactoryConfigurationError {
        try {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);
            FileOutputStream fos = new FileOutputStream("other.xml");
            StreamResult result = new StreamResult(fos);
            tr.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace(System.out);
        }
    }


}
