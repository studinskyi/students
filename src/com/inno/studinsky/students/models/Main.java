package com.inno.studinsky.students.models;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Student student = new Student(new GregorianCalendar(1994, Calendar.FEBRUARY, 8), 0l, "Conor", "John", "Reese");

        for (Field f : student.getClass().getDeclaredFields()) {
            System.out.println(f.getName() + " " + f.getType().toString());
        }

        for (Method method : student.getClass().getDeclaredMethods()) {
            System.out.println(method.getName() + " " + method.getReturnType().toString() + " " + method.getParameterTypes().length);
        }

        for (Annotation a : Student.class.getAnnotations()) {
            System.out.println(a.annotationType().toString() + " " + a.toString());
        }

        Field firstName = student.getClass().getDeclaredField("firstName");
        firstName.setAccessible(true);
        try {
            System.out.println(firstName.get(student));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //        Field stuId = student.getClass().getDeclaredField("id");
        //        stuId.setAccessible(true);
        //        //        Field modifiersField = stuId.class.getDeclaredField("modifiers");
        //        //        //stuId.setInt(stuId, stuId.getModifiers() & -Modifier.FINAL);
        //        stuId.set(null, 1l);
        //        System.out.println(stuId.get(student));
    }
}
