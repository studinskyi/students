package com.inno.studinsky.students.test;

import com.inno.studinsky.students.managers.Manager;
import com.inno.studinsky.students.managers.ManagerStudents;
import com.inno.studinsky.students.models.Group;
import com.inno.studinsky.students.models.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class ManagerStudentsTest {
    static Manager manager;
    static ManagerStudents managerStudents;
    static ArrayList<Group> groups;

    @Before
    void setUp() {
        groups = new ArrayList<>();
        groups.add(new Group("группа stc-6",6));
        manager = new Manager();
        managerStudents = manager.get_mStudents();
    }

    @After
    void tearDown() {
    }

    @Test
    void getAmountOfStudnts() {
    }

    @Test
    void addStudent() {
        int studentCounter = 0;
        Group gr1 = manager.addGroup("группа 1", 1);
        Student testStudent = managerStudents.addNewStudentToGroup(new GregorianCalendar(1994, Calendar.FEBRUARY, 8), "Petr", "Petrov", "Petrovich", gr1);
        //Student testStudent = new Student(new GregorianCalendar(1994, Calendar.FEBRUARY, 8), 1l, "Petr", "Petrov", "Petrovich");
        //managerStudents.addStudent(testStudent, 1);
        try {
            Field field = ManagerStudents.class.getDeclaredField("groups");
            field.setAccessible(true);
            HashMap<Long, Group> groups = (HashMap<Long, Group>) field.get(managerStudents);
            // получение студента из запрошенного через рефлексию списка и Stream API
            //groupList.stream().findFirst().equals(1);
            for (HashMap.Entry<Long, Group> entry : groups.entrySet())
                studentCounter = studentCounter + entry.getValue().getStudents().size();
                //            for (Group group : groupList) {
                //                //if (group.getId().equals(1)) {
                //                for (Student student : group.getStudents()) {
                //                    if (student.equals(testStudent)) {
                //                        studentCounter++;
                //                    }
                //                }
                //                //}
                //            }
            assertEquals(1, studentCounter);

        } catch (NoSuchFieldException e) {
            //e.printStackTrace();
            fail(e.getMessage());
            //assertTrue(false);
        } catch (IllegalAccessException e) {
            //e.printStackTrace();
            fail(e.getMessage());
            //assertTrue(false);
        }
    }

    @Test
    void getStudent() {
    }

    @Test
    void removeStudent() {
    }

}