package com.inno.studinsky.students.test;

import com.inno.studinsky.students.managers.ManagerGroups;
import com.inno.studinsky.students.managers.ManagerInterface;
import com.inno.studinsky.students.models.Group;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ManagerGroupsTest {
    static ManagerInterface managerGroupProxy;

    @BeforeEach
    void setUp() {
        managerGroupProxy = (ManagerInterface) Proxy.newProxyInstance(ManagerGroups.class.getClassLoader()
                , new Class[]{ManagerInterface.class}, new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("proxy metod call");
                        return new Group("группа stc-6",6);
                        //return method.invoke(proxy, args);
                    }
                });
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testAddGroup() {
        System.out.println("создание группы через Proxy " + ((Group)managerGroupProxy.addGroup(null, 1)).getNumberGroup());
    }

}