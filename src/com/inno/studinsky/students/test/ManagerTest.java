package com.inno.studinsky.students.test;

import com.inno.studinsky.students.managers.Manager;
import com.inno.studinsky.students.managers.ManagerInterface;
import com.inno.studinsky.students.models.Group;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class ManagerTest {
    static ManagerInterface managerGroupProxy;
    static ManagerInterface managerGroupMock;

    @BeforeAll
    static void setUp() {
        managerGroupMock = org.mockito.Mockito.mock(Manager.class);
        when(managerGroupMock.addGroup(null, 1)).thenReturn(new Group("группа stc-6", 6));

        //        managerGroupProxy = (ManagerInterface) Proxy.newProxyInstance(ManagerGroups.class.getClassLoader()
        //                , new Class[]{ManagerInterface.class}, new InvocationHandler() {
        //                    @Override
        //                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //                        System.out.println("proxy metod call");
        //                        return new Group(1, new ArrayList<Student>());
        //                        //return method.invoke(proxy, args);
        //                    }
        //                });
    }

    @AfterAll
    static void tearDown() {
    }

    @Test
    public void testAddGroup() {
        System.out.println(((Group) managerGroupMock.addGroup(null, 1)).getNumberGroup());
        //managerGroupProxy.addGroup(null);
    }

}